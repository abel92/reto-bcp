package pe.com.bcp.retobcp.util.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pe.com.bcp.retobcp.domain.CurrencyRequest;
import pe.com.bcp.retobcp.domain.CurrencyResponse;
import pe.com.bcp.retobcp.domain.model.CurrencyExchange;

@Mapper
public interface CurrencyExchangeMapper {
    CurrencyExchangeMapper INSTANCE = Mappers.getMapper(CurrencyExchangeMapper.class);

    CurrencyExchange CurrencyRequestToCurrencyExchange(CurrencyRequest request);

    @Mapping(source = "exchange", target = "currencyExchange")
    CurrencyResponse CurrencyExchangeToCurrencyResponse(CurrencyExchange currencyExchange);
}
