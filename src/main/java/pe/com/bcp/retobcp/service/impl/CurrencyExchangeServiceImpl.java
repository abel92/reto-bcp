package pe.com.bcp.retobcp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.bcp.retobcp.domain.CurrencyRequest;
import pe.com.bcp.retobcp.domain.CurrencyResponse;
import pe.com.bcp.retobcp.domain.model.CurrencyExchange;
import pe.com.bcp.retobcp.exception.CurrencyExchangeNotFoundException;
import pe.com.bcp.retobcp.repository.CurrencyExchangeRepository;
import pe.com.bcp.retobcp.service.CurrencyExchangeService;
import pe.com.bcp.retobcp.util.mapper.CurrencyExchangeMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

    private CurrencyExchangeRepository currencyExchangeRepository;

    @Autowired
    public CurrencyExchangeServiceImpl(CurrencyExchangeRepository currencyExchangeRepository) {
        this.currencyExchangeRepository = currencyExchangeRepository;
    }

    @Override
    public Mono<CurrencyResponse> apply(CurrencyRequest request) {
        return currencyExchangeRepository.findCurrencyExchangeByMoneySourceAndAndMoneyTarget(request.getMoneySource(), request.getMoneyTarget())
                .map(currencyExchange -> CurrencyExchangeMapper.INSTANCE.CurrencyExchangeToCurrencyResponse(currencyExchange))
                .map(currencyResponse -> this.calculateNewAmount(currencyResponse, request))
                .switchIfEmpty(Mono.error(() -> new CurrencyExchangeNotFoundException()));
    }

    @Override
    public Mono<CurrencyExchange> create(CurrencyRequest request) {
        CurrencyExchange exchange = CurrencyExchangeMapper.INSTANCE.CurrencyRequestToCurrencyExchange(request);
        return currencyExchangeRepository.save(exchange);
    }

    @Override
    public Mono<CurrencyExchange> update(CurrencyRequest request) {
        return currencyExchangeRepository.findCurrencyExchangeByMoneySourceAndAndMoneyTarget(request.getMoneySource(), request.getMoneyTarget())
                .doOnNext(currencyExchange -> currencyExchange.setExchange(request.getExchange()))
                .flatMap(currencyExchange -> currencyExchangeRepository.save(currencyExchange));
    }

    @Override
    public Flux<CurrencyExchange> getAll() {
        return currencyExchangeRepository.findAll();
    }

    public CurrencyResponse calculateNewAmount(CurrencyResponse CurrencyResponse, CurrencyRequest request) {
        CurrencyResponse.setAmount(request.getAmount());
        CurrencyResponse.setNewAmount(CurrencyResponse.getAmount() * CurrencyResponse.getCurrencyExchange());
        return CurrencyResponse;
    }
}
