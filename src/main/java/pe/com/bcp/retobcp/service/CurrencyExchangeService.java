package pe.com.bcp.retobcp.service;

import org.springframework.stereotype.Service;
import pe.com.bcp.retobcp.domain.CurrencyRequest;
import pe.com.bcp.retobcp.domain.CurrencyResponse;
import pe.com.bcp.retobcp.domain.model.CurrencyExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CurrencyExchangeService {
    Mono<CurrencyResponse> apply(CurrencyRequest request);

    Mono<CurrencyExchange> create(CurrencyRequest request);

    Mono<CurrencyExchange> update(CurrencyRequest request);

    Flux<CurrencyExchange> getAll();
}
