package pe.com.bcp.retobcp.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import pe.com.bcp.retobcp.domain.model.CurrencyExchange;
import reactor.core.publisher.Mono;

public interface CurrencyExchangeRepository extends ReactiveCrudRepository<CurrencyExchange, Long> {

    @Query("select id, money_source, money_target, exchange from currency_exchange where money_source = $1 and money_target = $2")
    Mono<CurrencyExchange> findCurrencyExchangeByMoneySourceAndAndMoneyTarget(String moneySource, String moneyTarget);

    /*@Query("select * from player where age = $1")
    Flux<CurrencyExchange> findByAge(int age);*/
}
