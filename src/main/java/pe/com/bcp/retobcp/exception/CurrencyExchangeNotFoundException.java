package pe.com.bcp.retobcp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CurrencyExchangeNotFoundException extends RuntimeException{
    public CurrencyExchangeNotFoundException() {
        super("¡could not find currency exchange!");
    }
}
