package pe.com.bcp.retobcp;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.r2dbc.core.DatabaseClient;
import pe.com.bcp.retobcp.domain.model.CurrencyExchange;
import pe.com.bcp.retobcp.repository.CurrencyExchangeRepository;
import reactor.core.publisher.Flux;

import java.util.stream.Stream;

@SpringBootApplication
public class RetobcpApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetobcpApplication.class, args);
    }

    @Bean
    ApplicationRunner init(CurrencyExchangeRepository repository, DatabaseClient client) {
        return args -> {
            client.execute("create table IF NOT EXISTS currency_exchange" +
                    "(id SERIAL PRIMARY KEY, " +
                    "money_source varchar (255) not null, " +
                    "money_target varchar (255) not null, " +
                    "exchange decimal(20,2) not null);")
                    .fetch().first().subscribe();

            client.execute("DELETE FROM currency_exchange;").fetch().first().subscribe();

            Stream<CurrencyExchange> stream = Stream.of(new CurrencyExchange(null, "USD", "PEN", 3.90));

            //initialize the database

            repository.saveAll(Flux.fromStream(stream))
                    .then()
                    .subscribe();

        };
    }
}
