package pe.com.bcp.retobcp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.bcp.retobcp.domain.CurrencyRequest;
import pe.com.bcp.retobcp.domain.CurrencyResponse;
import pe.com.bcp.retobcp.domain.model.CurrencyExchange;
import pe.com.bcp.retobcp.service.CurrencyExchangeService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/currency-exchange")
public class CurrencyExchangeController {

    private CurrencyExchangeService currencyExchangeService;

    @Autowired
    public CurrencyExchangeController(CurrencyExchangeService currencyExchangeService) {

        this.currencyExchangeService = currencyExchangeService;
    }

    @GetMapping("/apply")
    public Mono<CurrencyResponse> apply(@RequestBody CurrencyRequest request) {
        return currencyExchangeService.apply(request);
    }

    @GetMapping("/getAll")
    public Flux<CurrencyExchange> getAll() {
        return currencyExchangeService.getAll();
    }

    @PostMapping("/create")
    public Mono<CurrencyExchange> create(@RequestBody CurrencyRequest request) {
        return currencyExchangeService.create(request);
    }

    @PatchMapping("/update")
    public Mono<CurrencyExchange> update(@RequestBody CurrencyRequest request) {
        return currencyExchangeService.update(request);
    }
}
