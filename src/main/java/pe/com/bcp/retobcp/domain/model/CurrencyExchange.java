package pe.com.bcp.retobcp.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table
public class CurrencyExchange implements Serializable {
    @Id
    private Long id;
    private String moneySource;
    private String moneyTarget;
    private double exchange;
}

