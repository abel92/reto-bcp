package pe.com.bcp.retobcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CurrencyRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private double amount;
    private String moneySource;
    private String moneyTarget;
    private double exchange;
}
