package pe.com.bcp.retobcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CurrencyResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private double amount;
    private double newAmount;
    private String moneySource;
    private String moneyTarget;
    private double currencyExchange;
}
